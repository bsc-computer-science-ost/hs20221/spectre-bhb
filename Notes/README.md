# Cross-Privilege Spectre-v2 Attack

## Branch Prediciton

For example in an if-else-statement try to predict the path which will be true and save the result of the following instructions until the condition is calculated and the computer know which path is definitively true. If the prediction was wrong the saved result of instructions will be deleted and the other path will be calculated.

## Terminology

|         |                                                     |                                                              |
| ------- | --------------------------------------------------- | ------------------------------------------------------------ |
| BPU     | Branch Prediction Unit                              | - predicts upcomming branches<br />- logic for indirect and direct branches |
| BTB     | Branch Target Buffer                                | - is a cache (most recent target addresses; branch context)  |
| BTI     | Branch Target Injection                             | - also called Spectre-v2 or Spectre-BTB attack               |
| RSB     | Return Stack Buffer                                 |                                                              |
| IBPB    | Indirect Branch Prediction Barrier                  |                                                              |
| STIBP   | Single Thread Indirect Branch Predictors            |                                                              |
| (e)IBRS | (enhanced - ) Indirect Branch Resticted Speculation |                                                              |
| IBT     | Indirect Branch Tracking                            |                                                              |
| CET     | Control - Flow Enforcement Technology               |                                                              |
| BHI     | Branch History Injection                            |                                                              |
| eBPF    | Extended Barkeley Packet Filter                     | - Kernel Firewall                                            |
| BHB     | Branch History Buffer                               | - store hashes                                               |



## Defenses

### Software Defenses

| Technique         | Description                                                  | Processors                                               |
| ----------------- | ------------------------------------------------------------ | -------------------------------------------------------- |
| Generic retpoline | - standard software defense<br />- retpoline: special code sequence that converts an indirect branch to a "ret" instruction to ensure that RSB is used instead of BTB<br />=> no longer is indirect branch prediction performed | most Intel and AMD processors                            |
| AMD retpoline     | - alternative solution to generic retpoline<br />- every indirect branch is turned into a 'lfence/jmp' sequence: 'lfence' ensures that the load for the indirect branch target is retired before the 'jmp', this makes the residual transient window small enough to hinder exploitation | only on AMD (performanter as generic retpoline)          |
| ARM defenses      | - Invalidation of the branch predictor entries on mode switch by executing the BPIALL instruction or by disabling and re-enabling the MMU. It's implemented by the Secure Monitor Call 'SMCCC_ARCH_WORKAROUND_1'. | only on ARM (not supported for older microarchitectures) |



### Hardware Defenses

| Technique           | Description                                                  | Processors          |
| ------------------- | ------------------------------------------------------------ | ------------------- |
| Intel/AMD - IBPB    | - ensures that the execution of previous indirect branches cannot influence the prediction of subsequent branches<br />=> implements a barrier | Intel and AMD       |
| Intel/AMD - STIBP   | - restricts sharing of branch prediction state among hyperthreads on the same physical core | Intel and AMD       |
| Intel/AMD - (e)IBRS | - Ensure that indirect branch prediction cannot be controlled by software that is executed in a lower-privilege predictor mode (host-supervisor, host-user, guest-supervisor, guest-user).  The micro-code update activate 'IA32_SPEC_CTRL.IBRS' register on every mode switch. | Intel and AMD       |
| ARM - FEAT_CSV2     | - Similar to eIBRS protection but for ARM<br />- Register: 'ID_AA64PFRO_EL1' -> specify CSV2 field<br />- Supported:<br />(i) prevent the speculative control of indirect branch targets from different HW contents (CSV2 = 1)<br />(ii) enforce the same guarantees but for SW contexts by defined the 'SCXTNUM_ELx' register (CSV2 = 2) | ARM (since Armv8.5) |
| Intel - CET - IBT   | - only for Tiger Lake microcontrollerarchitectur<br />- IBT is part of CET<br />- CET-IBT prevents execution (speculative and architectural) without 'ENDBR32/64' instruction<br />=> do not prevent speculation altogether | Intel               |



## Branch History Injection

BHI is an "extension" of classic BTI in that it partially reintroduces the crossprivilege attack surface BTI lost after the introduction of eIBRS and CSV2. The BHI allows an attacker to mount BTI attack primitive even in presence of the in-silicon (eIBRS and CSV2) defenses. With this two defenses BTI is limited to only intra-mode attacks (e.g. kernel -> kernel), no inter-mode attacks are longer possible (e.g. user -> kernel).

![indirect branch prediction logic](./images/indirect-branch-prediction-logic.png)

F1: Hash Function
F2: Branch Context

### Bypassing eIBRS and CSV2

The tag of each BTB entry is hashed using the hardware context as a key, this conext hash also defines for which predictor mode the BTB entry is valid. With the following algorithm they tested to mixup the two seperate histories (H_load and H_dummy). They first filled the history with legitimate data and then set the dummy data to the load history instead of the dummy history. So a misprediction should result in a BTI, which it does in over 95% of all tested Intel and Arm devices. The following algorithm limiting ourselves the attack surface on intra-mode in-place BTI attacks.

![algorithm 1](./images/algorithm-1.png)

- Intel documentation: eIBRS does not protect against intra-mode BTI
- isolating the BTB entries would still allow same-mode mispredictions by design
- The intuition is that the accumulated branch history before a switch to a different privilege level is essential to accurately predict the initial branches and improve critical-path performance

*Observation 1: The BHB is not isolated among different privilege levels, allowing attackers to control indirect branch prediction for early branches in the higher-privilege mode.*

**Result:** Even with the state-of-art eIBRS in-silicon mitigation, an attacker can still hijack more privileged indirect branch predictions by controlling only the BHB value.

#### Expand Attack to out-of-place BTI

Now we try to understand how we can cause BTB-tag collisions between two distinct indirect branches by manipulating only the BHB value and not the branch address. For this primitive we would use gadgets accross targets of different kernel indirect branches to expand the attack surface. The following experiment is only tested on a same-predictor mode.

In the following figure you see two branches first we have a fixed history H_A and a fixed target T_A, while the second one uses a random history H_R for every iteration and a fixed target T_R. In this experiment we try to mispeculation from the random history H_R to the target T_A instead of the correct target T_R, which would indicate colliding BHB values.

![figure 4](./images/figure-4.png)

*Observation 2: On Intel systems, we can generate the same BTB tag for two distinct indirect branches by simply controlling the BHB value for one of these branches.*

**Result:** The expleriment successfully found colliding BHB values for all tested Intel CPUs, but not for any of the Arm and AMD processors in our test set. In other words, out-of-place BTI on Intel systems is possible just by controlling the BHB. On Arm in-place BTI attacks are possible.

=> in-place BTI attacks have essentially the same attack surface as out-of-place BTI ones

-> hier stecken geblieben Kapitel 5.3

### Exploitin BHI with eBPF

...

### Same-Predictor-Mode Exploit

...

### Exploitation Beyond eBPF

...



## Mitigations



