# Branch History Injection 

# Q&A

### What is the difference between Spectre-BHB, intra-mode BTI

**Spectre-BHB** since it was “Spectre-v2 aided by the Branch History Buffer (BHB)”. This primitive for Spectre-v2 mistraining can be exercised inter-mode (e.g., user→kernel) and also intra-mode (e.g., kernel→kernel). Intel proposed two names for the two attack models: they use Branch History Injection (BHI) to define the inter-mode history mistraining (CVE-2022-0001); and intra-mode BTI for exploits in which the specific branches used for mistraining and misprediction are located in the same privilege domain, regardless of the Branch History (CVE-2022-0002).

**Intra-mode BTI** same-mode Spectre-v2 attacks

### Is eIRBS, CSV2 broken?
Yes. They work as intended but the resiudal surface is much more siginicant than both vendor originally claimed.

## Glossary

| Word  | Description |
|-------|-------------|
| Branch Target Injection (BTI) |  Branch target injection works by causing an indirect branch to speculatively execute a ‘gadget’ which creates a side channel based on sensitive data available to the victim. |
| Branch Prediction | When a conditional operation such as an if…else statement needs to be processed, the branch predictor "speculates" which condition most likely will be met. It then executes the operations required by the most likely result ahead of time. This way, they're already complete if and when the guess was correct. At runtime, if the guess turns out not to be correct, the CPU executes the other branch of operation, incurring a slight delay. But if the guess was correct, speed is significantly increased. |  
| reptoline | Intels brancht target injetion mitigation | 
| Intel eIBRS | Effiecient hardware mitigation |
| ARM CSV2 | Effiecient hardware mitigation |
| Branch Prediction Unit  (BPU) | |
|  Extended Berkeley Packet Filter (eBPF) | Kernel technology, which lets programms run without needing to add additional modules or modify the kernel soruce code. It is basically a VM within the linux kernel. This does enable the developer to run bytecode that makes use of certain kernel resources. |
| Disclosure Gadet | A Disclosure Gadget is a sequence of
instructions that are speculatively executed, utilizing covertchannel techniques to measure the speculation
windows or the latency of data fetching, etc |
| Convert Communication Channels | A covert channel is any communication channel that can be exploited by a process to transfer information in a manner that violates the systems security policy. |

## Mitigations

-  In the absence of Simultaneous Multithreading (SMT), we verified that a com- plete flush of all previous BTB entries with IBPB on x86-64 can be used as an effective countermeasure.
-  


## Presentation Order

1. 


1. What is spectre -> Recap
2. Mitigations to Spectre
3. Branch History 
4. 
