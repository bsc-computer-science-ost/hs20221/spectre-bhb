# Introduction

## Privilege Spaces

The sepeartion between the user space and kernel space is often implemented with the help of protection rings. These are then implemented using different CPU modes.

- User Space: running applications (reffered to a user space or userland)
- Kernel Space: running a privileged operating kernel

### User Space
Has a virtual address space for every application running.

### Kernel Space
Has a single virtual address space.  Anything related to Process management, IO hardware management, and Memory management requires process to execute in Kernel mode.

![Kernel User Space](images/kerneluserspace.png)

## Syscalls
A system call is way for programs to interact with the operating system. A computer programm makes a system call when it makes a request to the operaitng system kernel. The parameters for the system call can be pushed on the stack or passed with the registers.

Types of System calls
- Process Control
- File Management
- Device Management
- Information Maintenance
- Communications

## Out of Order (OoO) Execution
Before the use of out of order exectuion we used in-order processes which worked normally. The next evolution has brought us out-of-order processors.

**In-Order Processor**

If one execution stalls, everything stalls.

- Instructions Fetch from RAM
- If operands are available (registry), the instruction is dispatched to the functional unit, or the processors stalls unit they are avialable.
- Instruction is executed
- Result is written back to the register file

**Out-of-Order Processor**

Idependent instructions behind a stalled instruction may pass it.

- Instructin Fetch
- Instruction dispatch to an instruction queue
- The instruction waits in the queue until its input operands are available. 
- The instructions is issued to the appropriate functional unit
- The results are qued
- Only after all older instructions have their results written back to the register file, then this result is written back to the register file. (retire in order)


**Out-of Order Exection**

Allows the processor to execute low level instructions further or in parallel with some instructions before the usual execution.


# Spectre-V1
## Attack
- modern processors use branch prediction and speculative execution to maximize performance because of limitation of code optimization
- spectre attacks involve inducing a victim to speculatively perform operations that would not occur during correct program execution and which leak the victim's  confidential information via a side channel to the adversary.
- extract unavailable secret information via side-channel attack
- Spectre attacks trick the processor into speculatively executing instruction sequences that should not have been executed under correct program execution.

- Microprocessors: Intel, AMD, ARM => bilion for devices

Attacks using Native Code:
- execute unavailable code in the same code file

Attacks using JavaScript and eBPF:
- spectre attacks can also be used to violate sandboxing, e.g. by mounting them via portable JavaScript code.


- At a high level, Spectre attacks violate memory isolation boundaries by combining speculative execution with data exfiltration via microarchitectural covert channels.
- cache-based convert channels (in this work):
    - Flush+Reload
    - Evict+Reload

Variant 1: Exploiting Conditional Branches
- Phase 1: initial mistraining phase
    - train with valid input that the if is often true
- Phase 2: Exploit Phase
    - use input that isn't true, the cpu guesses that the if statement will again be true -> branch will be stored in cache

=> Also when the cpu calculate the other path, the data is already in the cache and so the attacker can try to read the data from the cache. -> out-of-bounds read

Variant 2: Exploiting Indirect Branches
- based on return-oriented programming (ROP)
- attacker chooses a gadget from the victim's address space
- The attacker does not rely on a vulnerability in the victim code. Instead, the attacker trainss the Branch Target Buffer (BTB) to mispredict a branch from an indirect branch instruction to the address of the gadget. The gadget leak the information via a cache side channel.

- virtual address of the gadget during the attackers training need to match the victim's virutal address of the same gadget

Performance Improvements:
1. Out-of-order Execution
Allows the processor to execute instructions further or in parallel with some instructions before the usual execution.
2. Speculative Execution
The processor do not know future instuctions of the program by for example a conditional branch instuction. In this time the processor try to predict the path the program will follow, based on the register state, and speculatively execute instructions along the predicted path.
3. Branch Predicition
Better branch predictions improve performance by increasing the number of speculatively executed operations that can be successfully commited.
- indirect branch prediction needs at least two mechanisms for the prediction.
- BTB: keeps a mapping from addresses of recently executed branch instructions to destination addresses
- processors can use BTB for predict future code addresses
- Branch prediction logic of typically not shared across physical cores. Hence, the processor learns only from previous branches executed on the same core.
4. The Memory Hierarchy
- Caching is used to bridge the speed gap between the faster processor and the slower memory.
- Three cache levels (L1, L2, and L3)
5. Microarchitectural Side-Channel Attacks
If multiple programms are running at the same time at the same hardware, the branch predictions can affect the other programs on the same hardware. Initial microarchitectural side channel attacks exploited timing variablility and leakage through the L1 data cache to textract keys from cryptographic primitives.

- In this work we use Flush+Reload and Evict+Reload techniques.
THe main difference between the two techniques is the mahanism used for evicting the monitored cache line from the cache. In the Flush+Reload technique, the attacker uses a dedicated machine instruction, e.g. clflush to evict the line. Using Evict+Reload, eviction is achived by forcing contention on the cache set that stores the line e.g. by accessing other memory locations which are loaded into the cache and cause the processor to discoard (evict) the line that is subsequently probed.
6. Return-Oriented Programming (ROP)
Is a technique that allows an attacker who hijacks control flow to make a victim perfom complex operations by chaining together machine code snippets, called gadgets, found in the code of the vulnerable victim.


Attack Overview:
Step 1:
- help induce speculative execution, such as manipulating the cache state to remove data that the processor will need to determine the actual control flow.

Step 2:
- the processor speculatively executes instrution(s) that transfer confidential information from the victim context into a microarchitecural convert channel

Step 3:
For the final phase, the sensitive data is recovered. For Spectre attacks using Flush+Reload or Evict+Reload, the recovery process consists of timing the access to memory addresses in the cache lines being monitored.
Spectre attacks only assume that speculatively executed instructions can read from memory that the victim process could access normally, e.g., without triggering a page fault or exception.

IV: Variant 1: Exploiting conditional branch mispredicition

V: Variant 2: Poisoning Indirect Branches

### Intra-Mode

### Inter-Mode

### In-Place

### Out-Of-Place

## Mitigations

### Software

### Hardware
### Pipelineing
Pipelining is commonly used on modern processors for speeding up the execution of programs. Typically, the execution of a single instruction in the code can be partitioned to steps, starting with instruction fetch. While traditional processors allow the execution of one instruction at a time, a pipeline processor starts the execution of an instruction as soon as the previous instruction completes the first of these steps.

- Instruction fetches: Fetch the instruction from the main memory or cache.
- Instruction decoding: The processor interprets instruction and determines the operation that has to be performed.
- Operand fetch: If the execution of the instruction requires operand then the processor fetches operand from the main memory or cache.
- Instruction Execution: The processor performs the desired operation.
- Operand Store: The result of execution is stored.

![Pipelining visualized](images/../../images/pipelineing.jpeg)

**Pipeline Flush**

If we fail branch predicition and we need to flush our howle pipeline and start again.

### Speculative Execution
Intel introduced it with Pentium II.
Speculative exection has major flaws and therefore introduced serious security vulnerabilites like: Spectre, Meltdown, Foreshadow. Intel has opted to be more agressive and therefore suffered with the major secrutiy flaws it introduced.

The processor do not know future instuctions of the program by for example a conditional branch instuction. In this time the processor try to predict the path the program will follow, based on the register state, and speculatively execute instructions along the predicted path.

### Branch Prediction
The accuray of branch prediciton is very high. Better branch predictions improve performance by increasing the number of speculatively executed operations that can be successfully commited.

- Dynamic Branch Prediction
- Static Branch Prediction 
- Random Branch Prediction