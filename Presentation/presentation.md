# Cross-Privilege Spectre-v2 Attack

## Presentation Order

1. Code -> c, how does the compiler and the computer speed up the execution?
   1. Side Channel Attacks - What are Side Channel Attacks?
   2. User Kernel Space (Privilege Levels)
      1. What are the different privilege spaces?
      2. Syscalls
   3. Shared Memory
   4. Pipeline
   5. Branch Prediction
   6. Speculative Execution
2. SpectreV1 
   1. Attack
      1. Intra-, inter-mode and in-, out-of-place
   2. Mittigations:
      1. Software
      2. Hardware
3. SpectreV2
   1. Branch History Buffer (BHB)
   2. Branch History Injection
   3. Mitigations:
      1. Hardware
      2. Software

## Sync with Nathalie 27.10.22
- XKCD
- Reveresed, present the vulnerability first
- 