## Content - Marco

- Operating System Recap
- Spectre-V2
- Spectre-BHB
- Conclusion

## Side Channel Attacks - Marco

Beispiel: TMP verschlüsselt Daten für Überwachungskameras und dieser stromverbrauch kann verwendet werden, um zu wissen, wie viele Auto die Anlage passieren.

## User Kernel Space - Marco

User Mode
- Kein direkter Hardware-Zugang -> Für alles werden Syscalls verwendet (Treiber/IO)

Kernel Mode
- Unrestiktierter Zugriff auf alle Ressourcen des computers

## Perfomance Improvements - Petra

## Out-of-Order Execution - Petra

## Speculative Execution - Petra

## Branch Prediciton - Petra

## Return Oriented Programming - Petra

## Spectre-v2 - Marco

## Paper Goal - Marco

Was ist das Ziel des Papers?
- Beleuchtung der Spectre-V2 defenses
- Kann man Trotz der Defenses die Attacke noch ausführen

## BTI Attack Surface - Marco

- Spekulative Ausfürhung kann misverwendet werden
- Somit kann Daten geleakt werden

## BTI Criticality - Marco

Justification - Warum reden wir überhaupt über das Problem?
Weil die meisten Hersteller betroffen sind.

## Branch Target Injection - Marco

Das OS chached die verwendeten branches, damit die nächste Ausführung den Branch predicten kann.
Wir injecten in den Branch Target Buffer mit einem Gadet zu füllen und trainieren ihn nachher darauf, was anderes auszuführen (Gadet) mit welchem wir dann Daten exfiltieren können.

Conditional Branches -> no speculative execution
Indirect Branches -> speculative execution

## Mitigations - Marco

- Reptoline: Intel and AMD, indirect branch speculation unterbinden
- IBP
- STIBP
- eIBRS: Jump ist in einer secure enclave ausgeführt
- CSV2
- IBT

## Spectre BHB - Petra

## What is Spectre-BHB - Petra

## Why Spectre-BHB - Petra

## Who is affected by BHB - Petra

## BHI Attack Surface - Petra

## BPU - Branch Prediciton Unit - Petra

## BHB - Petra

## Branch Context - Petra 

## BTB - Petra

## Branch Prediction - Petra

## BPU - Petra

## BHI - Petra

## BHI Example - Marco

## Attacker Primitives - Marco

## Exploitation steps 1-5 - Marco

1. Triggering of eBPF gadet mit dem sender eines Packet zu einem eBPF packet filter -> eBPF hat viele Gadets
2. BTB generiet einen TAG für das zuvor verwendet Gadet
3. Attacker füllt von Userland den BHB, sodass wir eine Kollision haben
4. Unser Syscall wird misspredicted ins ebpf Gadet
5. S gadet leaked data in userland 

## Mitigations - Marco

- SMT: 
- Disable branch prediction in kernel code
- Disable eBPF

## Simultaneous Multithreading (SMT) - Marco

BTB flush bi jedem switch von userland in kernel mode

## Paper Goal - Marco

## Paper Conclusion - Marco

## Questions - Marco
